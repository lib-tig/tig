FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > tig.log'

COPY tig .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' tig
RUN bash ./docker.sh

RUN rm --force --recursive tig
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD tig
