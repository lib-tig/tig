User Environment
----------------

APT_CONFIG=/var/lib/sbuild/apt.conf
DEB_BUILD_OPTIONS=parallel=4
HOME=/sbuild-nonexistent
LANG=C.UTF-8
LC_ALL=C.UTF-8
LOGNAME=buildd
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
SCHROOT_ALIAS_NAME=build-PACKAGEBUILD-18802885
SCHROOT_CHROOT_NAME=build-PACKAGEBUILD-18802885
SCHROOT_COMMAND=env
SCHROOT_GID=2501
SCHROOT_GROUP=buildd
SCHROOT_SESSION_ID=build-PACKAGEBUILD-18802885
SCHROOT_UID=2001
SCHROOT_USER=buildd
SHELL=/bin/sh
TERM=unknown
USER=buildd
V=1

dpkg-buildpackage
-----------------

dpkg-buildpackage: info: source package tig
dpkg-buildpackage: info: source version 2.4.1-1ubuntu1
dpkg-buildpackage: info: source distribution focal
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 fakeroot debian/rules clean
dh_testdir
dh_testroot
rm -f build-stamp
/usr/bin/make clean
make[1]: Entering directory '/<<PKGBUILDDIR>>'
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
rm -f config.h config.log config.make config.status
rm -rf doc/manual.html-chunked/
rm -f doc/manual.pdf doc/manual.html doc/manual.toc doc/manual.tex
rm -f doc/tigmanual.7
rm -f doc/tig.1 doc/tig.1.html doc/tigrc.5 doc/tigrc.5.html
rm -f INSTALL.html NEWS.html README.html
dh_clean
 debian/rules build
dh_testdir
./configure --host=x86_64-linux-gnu --build=x86_64-linux-gnu \
	--prefix=/usr --mandir=\${prefix}/share/man --sysconfdir=/etc \
	CPPFLAGS="-Wdate-time -D_FORTIFY_SOURCE=2" CFLAGS="-g -O2 -fdebug-prefix-map=/<<PKGBUILDDIR>>=. -fstack-protector-strong -Wformat -Werror=format-security -Wall -g -I/usr/include/ncursesw -O2" LDFLAGS="-Wl,-Bsymbolic-functions -Wl,-z,relro"
checking for x86_64-linux-gnu-gcc... x86_64-linux-gnu-gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether x86_64-linux-gnu-gcc accepts -g... yes
checking for x86_64-linux-gnu-gcc option to accept ISO C89... none needed
checking how to run the C preprocessor... x86_64-linux-gnu-gcc -E
checking for grep that handles long lines and -e... /bin/grep
checking for egrep... /bin/grep -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking execinfo.h usability... yes
checking execinfo.h presence... yes
checking for execinfo.h... yes
checking paths.h usability... yes
checking paths.h presence... yes
checking for paths.h... yes
checking for stdint.h... (cached) yes
checking for stdlib.h... (cached) yes
checking for string.h... (cached) yes
checking sys/time.h usability... yes
checking sys/time.h presence... yes
checking for sys/time.h... yes
checking for unistd.h... (cached) yes
checking wordexp.h usability... yes
checking wordexp.h presence... yes
checking for wordexp.h... yes
checking for gettimeofday... yes
checking whether environ is declared... no
checking whether errno is declared... yes
checking for mkstemps... yes
checking for setenv... yes
checking for strndup... yes
checking for wordexp... yes
checking for x86_64-linux-gnu-pkg-config... no
checking for pkg-config... no
checking for ncursesw via pkg-config... no
checking for ncursesw via fallback... 
checking for initscr() with -lncursesw... yes
checking for nodelay() with -lncursesw... yes
checking for working ncursesw/curses.h... yes
checking for working ncursesw.h... no
checking for working ncurses.h... yes
checking for tgetent... no
checking for tgetent in -lncursesw... yes
checking which library has the termcap functions... using libncursesw
checking readline/readline.h usability... yes
checking readline/readline.h presence... yes
checking for readline/readline.h... yes
checking readline/history.h usability... yes
checking readline/history.h presence... yes
checking for readline/history.h... yes
checking version of installed readline library... 8.0
checking for iconv... yes
checking for iconv declaration... 
         extern size_t iconv (iconv_t cd, char * *inbuf, size_t *inbytesleft, char * *outbuf, size_t *outbytesleft);
checking for gsed... no
checking for asciidoc... asciidoc
checking for xmlto... xmlto
checking for docbook2pdf... docbook2pdf
configure: creating ./config.status
config.status: creating config.make
config.status: creating config.h
dh_testdir
/usr/bin/make all doc
make[1]: Entering directory '/<<PKGBUILDDIR>>'
        CC  src/tig.o
        CC  src/types.o
        CC  src/string.o
        CC  src/util.o
        CC  src/map.o
        CC  src/argv.o
        CC  src/io.o
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from src/io.c:14:
In function ‘strncpy’,
    inlined from ‘encoding_open’ at src/io.c:52:2:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying as many bytes from a string as its length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
src/io.c: In function ‘encoding_open’:
src/io.c:41:15: note: length computed here
   41 |  size_t len = strlen(fromcode);
      |               ^~~~~~~~~~~~~~~~
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from src/io.c:14:
In function ‘strncpy’,
    inlined from ‘io_from_string’ at src/io.c:649:2:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying as many bytes from a string as its length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
src/io.c: In function ‘io_from_string’:
src/io.c:639:15: note: length computed here
  639 |  size_t len = strlen(str);
      |               ^~~~~~~~~~~
        CC  src/refdb.o
       GEN  src/builtin-config.c
        CC  src/builtin-config.o
        CC  src/request.o
        CC  src/line.o
        CC  src/keys.o
        CC  src/repo.o
        CC  src/options.o
        CC  src/draw.o
        CC  src/prompt.o
        CC  src/display.o
src/display.c: In function ‘update_status_window’:
src/display.c:474:4: warning: ‘vwprintw’ is deprecated [-Wdeprecated-declarations]
  474 |    vwprintw(status_win, msg, args);
      |    ^~~~~~~~
In file included from include/tig/tig.h:77,
                 from src/display.c:14:
/usr/include/curses.h:825:28: note: declared here
  825 | extern NCURSES_EXPORT(int) vwprintw (WINDOW *, const char *,va_list) GCC_DEPRECATED(use vw_printw); /* implemented */
      |                            ^~~~~~~~
        CC  src/view.o
        CC  src/search.o
        CC  src/parse.o
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from src/parse.c:14:
In function ‘strncpy’,
    inlined from ‘get_path’ at src/parse.c:264:3:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying as many bytes from a string as its length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
src/parse.c: In function ‘get_path’:
src/parse.c:264:3: note: length computed here
  264 |   strncpy(entry->path, path, strlen(path));
      |   ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        CC  src/watch.o
        CC  src/pager.o
        CC  src/log.o
        CC  src/diff.o
        CC  src/help.o
        CC  src/tree.o
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from include/tig/util.h:17,
                 from src/tree.c:14:
In function ‘strncpy’,
    inlined from ‘tree_entry’ at src/tree.c:132:2:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying as many bytes from a string as its length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
src/tree.c: In function ‘tree_entry’:
src/tree.c:132:2: note: length computed here
  132 |  strncpy(entry->name, path, strlen(path));
      |  ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        CC  src/blob.o
        CC  src/blame.o
        CC  src/refs.o
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from include/tig/io.h:17,
                 from src/refs.c:14:
In function ‘strncpy’,
    inlined from ‘refs_open’ at src/refs.c:158:3:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying 14 bytes from a string of the same length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        CC  src/status.o
        CC  src/stage.o
        CC  src/main.o
In file included from /usr/include/string.h:494,
                 from include/tig/tig.h:46,
                 from include/tig/repo.h:17,
                 from src/main.c:14:
In function ‘strncpy’,
    inlined from ‘main_add_commit’ at src/main.c:93:2:
/usr/include/x86_64-linux-gnu/bits/string_fortified.h:106:10: warning: ‘__builtin_strncpy’ output truncated before terminating nul copying as many bytes from a string as its length [-Wstringop-truncation]
  106 |   return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
src/main.c: In function ‘main_add_commit’:
src/main.c:86:13: note: length computed here
   86 |  titlelen = strlen(title);
      |             ^~~~~~~~~~~~~
        CC  src/stash.o
        CC  src/grep.o
        CC  src/ui.o
        CC  src/apps.o
        CC  src/graph.o
        CC  src/graph-v1.o
        CC  src/graph-v2.o
        CC  compat/hashtab.o
        CC  compat/utf8proc.o
      LINK  src/tig
        CC  test/tools/test-graph.o
      LINK  test/tools/test-graph
        CC  tools/doc-gen.o
      LINK  tools/doc-gen
  ASCIIDOC  doc/tig.1.xml
     XMLTO  doc/tig.1
Warn: meta author : no refentry/info/author                        tig
Note: meta author : see http://docbook.sf.net/el/author            tig
Warn: meta author : no author data, so inserted a fixme            tig
Note: Writing tig.1
  ASCIIDOC  doc/tigrc.5.xml
     XMLTO  doc/tigrc.5
Warn: meta author : no refentry/info/author                        tigrc
Note: meta author : see http://docbook.sf.net/el/author            tigrc
Warn: meta author : no author data, so inserted a fixme            tigrc
Note: Writing tigrc.5
  ASCIIDOC  doc/tigmanual.7.xml
     XMLTO  doc/tigmanual.7
Warn: meta author : no refentry/info/author                        tigmanual
Note: meta author : see http://docbook.sf.net/el/author            tigmanual
Warn: meta author : no author data, so inserted a fixme            tigmanual
Note: Writing tigmanual.7
  ASCIIDOC  doc/tig.1.html
  ASCIIDOC  doc/tigrc.5.html
       GEN  doc/manual.toc
  ASCIIDOC  doc/manual.html
  ASCIIDOC  README.html
  ASCIIDOC  INSTALL.html
  ASCIIDOC  NEWS.html
  ASCIIDOC  doc/manual.xml
     XMLTO  doc/manual.html-chunked
Writing ar01s02.html for section(viewer)
Writing ar01s03.html for section(env-variables)
Writing ar01s04.html for section(keys)
Writing ar01s05.html for section(refspec)
Writing ar01s06.html for section(_more_information)
Writing ar01s07.html for section(copy-right)
Writing ar01s08.html for section(references)
Writing index.html for article
    DB2PDF  doc/manual.pdf
Using catalogs: /etc/sgml/catalog
Using stylesheet: /usr/share/docbook-utils/docbook-utils.dsl#print
Working on: /<<PKGBUILDDIR>>/doc/manual.xml
Done.
rm doc/tigmanual.7.xml doc/manual.xml doc/tig.1.xml doc/tigrc.5.xml
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
touch build-stamp
 fakeroot debian/rules binary
# nothing to do here
dh_testdir
dh_testroot
dh_prep
dh_installdirs
/usr/bin/make install install-doc DESTDIR=/<<PKGBUILDDIR>>/debian/tig
make[1]: Entering directory '/<<PKGBUILDDIR>>'
   INSTALL  src/tig -> /<<PKGBUILDDIR>>/debian/tig/usr/bin
   INSTALL  tigrc -> /<<PKGBUILDDIR>>/debian/tig/etc
   INSTALL  doc/tig.1 -> /<<PKGBUILDDIR>>/debian/tig/usr/share/man/man1
   INSTALL  doc/tigrc.5 -> /<<PKGBUILDDIR>>/debian/tig/usr/share/man/man5
   INSTALL  doc/tigmanual.7 -> /<<PKGBUILDDIR>>/debian/tig/usr/share/man/man7
   INSTALL  doc/tig.1.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
   INSTALL  doc/tigrc.5.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
   INSTALL  doc/manual.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
   INSTALL  README.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
   INSTALL  INSTALL.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
   INSTALL  NEWS.html -> /<<PKGBUILDDIR>>/debian/tig/usr/share/doc//tig
make[1]: Leaving directory '/<<PKGBUILDDIR>>'
# don't ship install instructions; README only links to them
rm -f debian/tig/usr/share/doc/tig/INSTALL.html \
	debian/tig/usr/share/doc/tig/README.html
mkdir -p debian/tig/usr/share/bash-completion/completions
cp contrib/tig-completion.bash debian/tig/usr/share/bash-completion/completions/tig
chmod 644 debian/tig/usr/share/bash-completion/completions/tig
dh_testdir
dh_testroot
dh_installchangelogs NEWS.adoc
dh_installdocs doc/manual.pdf
dh_installexamples contrib/*.tigrc
dh_link
dh_strip
dh_compress -X.pdf
dh_fixperms
dh_installdeb
dh_shlibdeps
dh_gencontrol
dh_md5sums
dh_builddeb
INFO: pkgstriptranslations version 144
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing tig-dbgsym (in debian/.debhelper/tig/dbgsym-root); do_strip: , oemstrip: 
pkgstriptranslations: processing tig (in debian/tig); do_strip: , oemstrip: 
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/.debhelper/tig/dbgsym-root/DEBIAN/control, package tig-dbgsym, directory debian/.debhelper/tig/dbgsym-root
dpkg-deb: building package 'tig-dbgsym' in 'debian/.debhelper/scratch-space/build-tig/tig-dbgsym_2.4.1-1ubuntu1_amd64.deb'.
pkgstripfiles: processing control file: debian/tig/DEBIAN/control, package tig, directory debian/tig
.. removing usr/share/doc/tig/changelog.gz
pkgstripfiles: Truncating usr/share/doc/tig/changelog.Debian.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package tig ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'tig' in '../tig_2.4.1-1ubuntu1_amd64.deb'.
	Renaming tig-dbgsym_2.4.1-1ubuntu1_amd64.deb to tig-dbgsym_2.4.1-1ubuntu1_amd64.ddeb
 dpkg-genbuildinfo --build=binary
 dpkg-genchanges --build=binary -mLaunchpad Build Daemon <buildd@lgw01-amd64-024.buildd> >../tig_2.4.1-1ubuntu1_amd64.changes
dpkg-genchanges: info: binary-only upload (no source code included)
 dpkg-source --after-build .
